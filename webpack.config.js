module.exports = {
    entry: './app/Client/js/index.js',
    output: {
        path: __dirname + "/app/Client/build",
        filename: 'bundle.js'
    },
    watch: true,
    devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            }
        ]
    }
};
