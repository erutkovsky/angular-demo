import homeFactory from '../services/home.factory.js';
import workloadService from '../services/workload.service.js';
import userService from '../services/user.service.js';

export default angular.module('app').controller('Home', function(homeFactory, workloadService, userService) {
    let self = this;

    self.user = userService.getUser();

    self.userNameChange = (name) => {
        userService.setFullName(name);
    };



    self.maxHoursMonth = 176;
    self.maxHoursWeek = 40;

    self.listHome = workloadService.getList();
    self.currentHoursMonth = homeFactory.currentHoursMonth(self.listHome);
    self.currentHoursWeek = homeFactory.currentHoursWeek(self.listHome[0]);
});
