import userService from '../services/user.service.js';

export default angular.module('app').controller('User', function(userService, $state) {

    let self = this;

    self.user = userService.getUser();

    self.submit = () => {
        userService.setUser(self.login, self.password);
        $state.go('index.home');
    }

});
