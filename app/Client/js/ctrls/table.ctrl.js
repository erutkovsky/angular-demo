import table from '../directives/table.directive.js';
import listFactory from '../services/list.factory.js';
import workloadService from '../services/workload.service.js';

export default angular.module('app').controller('Table', function(listFactory, workloadService) {

    let self = this;

    self.listTable = workloadService.getList();

    self.totalItems = self.listTable.length;

    self.currentPage = 2;
    self.isEnablePrev = true;
    self.isEnableNext = true;

    self.setCurrent = () => {
        self.currentPage = 1;
        self.isEnablePrev = false;
        angular.element(document.getElementById('prevPage')).addClass('disabled');
        self.isEnableNext = true;
        angular.element(document.getElementById('nextPage')).removeClass('disabled');
    };

    self.prevPage = () => {
        self.currentPage--;
        self.isEnableNext = true;
        angular.element(document.getElementById('nextPage')).removeClass('disabled');
        if (self.currentPage === 1) {
            angular.element(document.getElementById('prevPage')).addClass('disabled');
            self.isEnablePrev = false;
        }
    };

    self.nextPage = () => {
        self.currentPage++;
        self.isEnablePrev = true;
        angular.element(document.getElementById('prevPage')).removeClass('disabled');
        if (self.currentPage === self.totalItems) {
            angular.element(document.getElementById('nextPage')).addClass('disabled');
            self.isEnableNext = false;
        }
    };

    self.save = () => {
        listFactory.serverSetList(self.listTable);
    };

    self.updateTable = () => {
        if (!angular.equals(self.listTable, listFactory.storageGetList())) {
            listFactory.storageSetList(self.listTable)
        }
    }
});
