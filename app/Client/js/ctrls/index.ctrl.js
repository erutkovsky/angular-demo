import userService from '../services/user.service.js';

export default angular.module('app').controller('Index', function(userService, $state) {
    let self = this;
    this.today = new Date().toDateString();
    
    if (!userService.isLogin) {
        $state.go('login');
    }
});
