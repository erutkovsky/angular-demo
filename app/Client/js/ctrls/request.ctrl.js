import illnessTab from '../directives/illness.directive.js';
import overtimeTab from '../directives/overtime.directive.js';
import vacationTab from '../directives/vacation.directive.js';
import wopayTab from '../directives/wopay.directive.js';
import datepicker from '../directives/datepicker.directive.js';
import balance from '../directives/balance.directive.js';

export default angular.module('app').controller('Request', function ($scope) {
    let self = this;
    let oneDay = 24 * 60 * 60 * 1000;
    self.end = new Date();
    self.start = new Date();

    $scope.$watch('request.start', (v, old) => {
        self.endOption.minDate = v;
        if (self.end.getDay() != self.startOption.maxDate && old.getDay() == self.end.getDay()) {
            self.end = v;
        }
        self.duration = Math.round(Math.abs((self.end.getTime() - self.start.getTime()) / (oneDay))) + 1;
    });

    $scope.$watch('request.end', (v) => {
        if (self.start.getDay() != self.end.getDay()) {
            self.startOption.maxDate = v;
        }
        self.duration = Math.round(Math.abs((self.end.getTime() - self.start.getTime()) / (oneDay))) + 1;
    });

    self.openStart = function () {
        self.startOpened = true;
    };

    self.openEnd = function () {
        self.endOpened = true;
    };

    self.endOption = {
        yearFormat: "'yy'",
        startingDay: 1,
        minDate: null,
        maxDate: new Date('11/11/2020')
    };

    self.startOption = {
        yearFormat: "'yy'",
        startingDay: 1,
        minDate: new Date(),
        maxDate: new Date('11/11/2020')
    };
});
