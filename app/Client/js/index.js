import module from './module/module.js';
import indexCtrl from './ctrls/index.ctrl.js';
import homeCtrl from './ctrls/home.ctrl.js';
import tableCtrl from './ctrls/table.ctrl.js';
import reqCtrl from './ctrls/request.ctrl.js';
import userCtrl from './ctrls/user.ctrl.js';
