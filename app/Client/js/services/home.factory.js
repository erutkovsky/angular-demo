export default angular.module('app').factory('homeFactory', () => {
    let currentHoursMonth = (list) => {
        let hours = 0;
        for (let element of list) {
            hours = element.production.reduce((sum, current) => {
                return sum + current;
            }, hours);
        }
        return hours;
    };

    let currentHoursWeek = (week) => {
        let hours = 0;
        hours = week.production.reduce((sum, current) => {
            return sum + current;
        }, hours);
        return hours;
    };

    return {
        currentHoursMonth,
        currentHoursWeek
    };
});
