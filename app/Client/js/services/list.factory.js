import serverFactory from './serverAPI.factory.js';
import storageFactory from './localstorageAPI.factory.js';

export default angular.module('app').factory('listFactory', ($q, serverFactory, storageFactory) => {

    let storageGetList = () => storageFactory.getData();

    let storageSetList = (list) => storageFactory.setData(list);

    let serverGetList = () => serverFactory.getData()
                                .then(data => {
                                    if (data.status === 200) {
                                        return data.data;
                                    } else {
                                        throw new Error();
                                    }
                                })
                                .catch();

    let serverSetList = (list) => serverFactory.postData(list);

    let initList = () => {
        return $q((resolve, reject) => {
            if (storageGetList()) {
                resolve(storageGetList());
            } else {
                serverGetList()
                    .then((list) => {
                        resolve(list);
                        serverSetList(list);
                    })
                    .catch((error) => {
                        reject(error);
                    });
            }
        });
    };


    return {
        storageGetList,
        storageSetList,
        serverGetList,
        serverSetList,
        initList
    };
});
