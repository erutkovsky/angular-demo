export default angular.module('app').factory('serverFactory', ($http) => {

    let getData = () => $http.get('/list');

    let postData = list => $http.post('/list', list);

    return {
        getData,
        postData
    }

});
