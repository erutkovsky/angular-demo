export default angular.module('app').factory('storageFactory', ($http) => {

    let getData = () => JSON.parse(localStorage.getItem('list'));

    let setData = list => localStorage.setItem('list', JSON.stringify(list));

    return {
        getData,
        setData
    }

});
