export default angular.module('app').service('userService', function (listFactory) {

    this.user = {};

    this.isLogin = false;

    this.setUser = (name, password) => {
        this.isLogin = true;
        this.user.name = name;
        this.user.password = password;
    }

    this.getUser = () => {
        return this.user;
    }

    this.setFullName = (name) => {
        this.user.name = name;
    }
});
