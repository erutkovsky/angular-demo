import listFactory from './list.factory.js';

export default angular.module('app').service('workloadService', function (listFactory) {

    this.list = [];

    this.pushList = (item) => {
        this.list.push(item);
    }

    this.getList = () => {
        return this.list;
    }

    this.init = () => {
        return listFactory.initList()
            .then(list => {
                this.list = list;
                return list;
            })
            .catch();
    }
});
