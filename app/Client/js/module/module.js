import angular from 'angular';
import router from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';

export default angular.module('app', [router, uiBootstrap])
    .config(($stateProvider, $urlRouterProvider) => {
        $urlRouterProvider.otherwise('/login');
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.tmpl.html',
                controller: 'User',
                controllerAs: 'user'
            })
            .state('index', {
                abstract: true,
                templateUrl: 'templates/index.tmpl.html',
                controller: 'Index',
                controllerAs: 'index',
                resolve: {
                    listInit: function (workloadService) {
                        return workloadService.init();
                    }
                }
            })
            .state('index.request', {
                url: '/request',
                templateUrl: 'templates/request.tmpl.html',
                controller: 'Request',
                controllerAs: 'request'
            })
            .state('index.home', {
                url: '/home',
                templateUrl: 'templates/home.tmpl.html',
                controller: 'Home',
                controllerAs: 'home'
            })
            .state('index.workhours', {
                url: '/workhours',
                templateUrl: 'templates/workhours.tmpl.html',
                controller: 'Table',
                controllerAs: 'tbl'
            })
            .state('index.vacation', {
                url: '/vacation',
                templateUrl: 'templates/vacation.tmpl.html',
                controller: 'Request',
                controllerAs: 'request'
            })
    });
