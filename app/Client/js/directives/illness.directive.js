export default angular.module('app').directive('illnessTab', () => {
    return {
        restrict: 'EA',
        templateUrl: '../../templates/tabs/illness.tab.tmpl.html'
    };
});
