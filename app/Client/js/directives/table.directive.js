export default angular.module('app').directive('workhoursTable', () => {
    return {
        restrict: 'EA',
        templateUrl: '../../templates/directives/table.tmpl.html',
        controller: 'Table',
        controllerAs: 'tbl'
    };
});
