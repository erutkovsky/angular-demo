export default angular.module('app').directive('balance', () => {
    return {
        restrict: 'EA',
        templateUrl: '../../templates/directives/balance.tmpl.html'
    };
});
