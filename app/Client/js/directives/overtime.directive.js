export default angular.module('app').directive('overtimeTab', () => {
    return {
        restrict: 'EA',
        templateUrl: '../../templates/tabs/overtime.tab.tmpl.html'
    };
});
