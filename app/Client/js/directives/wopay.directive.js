export default angular.module('app').directive('wopayTab', () => {
    return {
        restrict: 'EA',
        templateUrl: '../../templates/tabs/wopay.tab.tmpl.html'
    };
});
