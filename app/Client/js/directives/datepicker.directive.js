export default angular.module('app').directive('datepickerRow', () => {
    return {
        restrict: 'EA',
        templateUrl: '../../templates/directives/datepicker.tmpl.html'
    };
});
