export default angular.module('app').directive('vacationTab', () => {
    return {
        restrict: 'EA',
        templateUrl: '../../templates/tabs/vacation.tab.tmpl.html'
    };
});
