'use strict';
const router = require('express').Router();
const fs = require('fs');
const bodyParser = require('body-parser');

router.use((req, res, next) => {
  console.log('list route');
  next();
});

router.get('/', (req, res) => {

    let flag = true;

    let jsonData = JSON.parse(fs.readFileSync('app/Server/data.json'));

    if (flag) {
        res.status(200).send(jsonData);
    } else {
        res.status(400).send('smth bad with promise');
    }
});

router.post('/', (req, res) => {
    console.log(req.user);
    fs.writeFile('app/Server/data.json', JSON.stringify(req.body), (err) => {
        if (err) {
            console.error('bad 400');
            return res.sendStatus(400);
        } else {
            console.log('good 200');
            return res.sendStatus(200);
        }
    });
});

module.exports = router;
