'use strict';
const router = require('express').Router();
const list = require('./routes/list.route.js');

router.use('/list', list);

module.exports = router;
