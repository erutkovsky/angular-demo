'use strict';
const express = require('express');
const passport = require('passport');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const router = require('./router.js');

const app = express();

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/angulardemo');

app.use(express.static('./app/Client/'));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
app.use(router);

app.listen(8080, console.log('listen 8080'));